@extends('main')
@section('title','|HomePage')
@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="jumbotron">
            <h1> Smart Parking</h1>
            <p class="lead"> You can Find Parking Spots For Your Cars Using this Web Application.!</p>
          </div>
        </div>
      </div>  <!-- end of header .row -->
      <div class="row"> <!--start of body row-->
        <div class="col-md-8">
          <h3>What Do We achieve?</h3>
          <p> Parking has been a problem for many who reside in cities. Searching for parking space and spending the precious time instead of doing what we actually should have been doing. 

            <h2>Introducing Smart Parking System </h2>
              <p> Here we help you book your parking space before  hand so that the space is saved for you and you can get to your meeting in time. 
              </p>
          </p>
        </div>
        <div class="col-md-3 col-md-offset-1">
          <h2>Sidebar</h2>
        </div>
      </div><!--End of body row-->
@endsection