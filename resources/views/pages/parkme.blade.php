@extends('main')
@section('title','|Parking APP')
@section('stylesheets','<link rel="stylesheet" type="text/css" href="block.css">')
  
@section('content')

      <div class="row"> <!--start of body row-->
        <div class="col-md-8">
          <h3>Select Your Parking Area</h3>
          <a href="" id="1"
          style=" display:inline-block;
            width:200px;
            height:200px;
            background-color:#ccc;
            border:1px solid #ff0000;"></a>
             <a href="" id="2" 
          style=" display:inline-block;
            width:200px;
            height:200px;
            background-color:#ccc;
            border:1px solid #ff0000;"></a>
             <a href="" id="3"
          style=" display:inline-block;
            width:200px;
            height:200px;
            background-color:#ccc;
            border:1px solid #ff0000;"></a>
        </div>
        <div class="col-md-3 col-md-offset-1">
          <h2>Sidebar</h2>
            <p>For Slot <span id="slotid">1</p>
            Increase time by: <input type="text"><br>
            Remaining time: <span id="remaining"></span>
            </p>
            <input type="button" class="btn" value="Increase" onclick="increase()" /><br><br>
            <input type="button" class="btn" value="Reset" onclick="reset()" />
            
        </div>
      </div><!--End of body row-->
@endsection
@section('scripts')
<script type="text/javascript">
  var slot1={
    time:30;
    parked:true;
  }
    var slot2={
    time:10;
    parked:false;
  }
    var slot3={
    time:20;
    parked:true;
  }

  var slot="slot";
  function display1(){
    document.getElementById('remaining').innerHTML=slot1.time;
  }
   function display2(){
    document.getElementById('remaining').innerHTML=slot2.time;
  }

   function display3(){
    document.getElementById('remaining').innerHTML=slot3.time;
  }

var elements = document.getElementsByTagName("a"); 
for(var i=0; i<elements.length; i++){
    if (elements[i].id == i) { 
         elements[i].onclick = function(){ 
           document.getElementById(remaining).innerHTML=slot1.time; 
   }
 } 
}

</script>
@endsection