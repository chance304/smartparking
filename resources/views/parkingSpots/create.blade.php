@extends('main')
@section('title','|New Spot')
@section('stylesheets')
{!!Html::style('css/parsley.css')!!}
@endsection
@section('content')
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>Create New Spot</h1>
			<hr>
			{!! Form::open(['route' => 'parkingspots.store','data-parsley-validate'=>'']) !!}
			{{-- should match database field--}}
			{{Form::label('spotLocation','Spot Location:')}}
    		{{Form::text('spotLocation',null,array('class'=>'form-control','maxlength'=>'255'))}} 

    		{{Form::label('spot_name','Spot Name: ')}}
    		{{Form::text('spot_name',null,array('class'=>'form-control','required'=>''))}}

    		{{Form::label('no_of_spots','No of Spots: ')}}
    		{{Form::text('no_of_spots',null,array('class'=>'form-control','required'=>''))}}


    		{{Form::submit('Create ParkingSpot',array('class'=>'btn btn-success btn-lg btn-block','style'=>'margin-top:20px'))}}
			{!! Form::close() !!}
		</div>
	</div>
		
@endsection
@section('scripts')
{!!Html::script('js/parsley.min.js')!!}
@endsection