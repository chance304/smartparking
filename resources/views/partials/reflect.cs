using System;
using System.Reflection;
namespace Reflected
{
	public class MainClass
	{
		public static void Main(String [] args)
		{
			Type t = Type.GetType("Reflected.Customer");
			Console.WriteLine("Full Name : {0}",t.FullName);
			Console.WriteLine("Just Name : {0}",t.Name);
			Console.WriteLine("Just Namespace : {0}",t.Namespace);
			Console.WriteLine();

			Console.WriteLine("Properties: ");
			PropertyInfo[] properties = t.GetProperties();
			foreach(PropertyInfo property in properties ){
				Console.WriteLine(property.PropertyType.Name +" "+property.Name)
			}

			Console.WriteLine();
			Console.WriteLine("Methods in Customer: ");
			MethodInfo[] methods = t.GetMethods();
			foreach(MethodInfo method in methods ){
				Console.WriteLine(property.ReturnType.Name +" "+Method.Name)
			}
		} 
	}

	public class Customer
	{
		public int id { get; set;}
		public string name { get; set;}

		public Customer(){
			this.id=-1;
			this.name=string.empty();
		}

		public Customer(int id, string name)
		{
			this.id = id;
			this.name = name;
		}

		public void printId()
		{
			Console.WriteLine("Id: {0}",this.id);
		}

		public void printName()
		{
			Console.WriteLine("Name: {0}",this.name);
		}
	}
}