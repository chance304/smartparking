<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Authentication Routes:
Route::group(['middleware' => ['web']], function() {
 
// Login Routes...
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showgetLogin']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@postLogin']);
    Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
 
// Registration Routes...
    Route::get('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
    Route::post('register', ['as' => 'register.post', 'uses' => 'Auth\RegisterController@register']);
 
// Password Reset Routes...
    Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
    Route::get('password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
    Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);
});
Route::get('contact', 'PagesController@getContact');
Route::get('about', 'PagesController@getAbout');
Route::get('/', 'PagesController@getIndex');
Route::get('/smartparking','PagesController@getSmartParking');
Route::get('parkme','PagesController@getParkMe');
Route::resource('parkingspots','ParkingSpotsController');
Route::get('/addslot','ParkingSpotsController@create');